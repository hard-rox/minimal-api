﻿using Microsoft.EntityFrameworkCore;
using MinimalApi.Domain.Entities;
using MinimalApi.Infrastructure.Services;
using System.Threading.Tasks;
using Xunit;

namespace MinimalApi.Tests
{
    //[CollectionDefinition("AuthorServiceTest", DisableParallelization = true)]
    public class AuthorServiceTest
    {
        [Fact]
        public async Task Create_Normally_CreatesAnAuthor_ReturnsTrue()
        {
            var context = TestFactory.GetContext();
            var author = new Author() { Name = "Test Author" };
            var service = new AuthorService(context);

            var result = await service.Create(author);
            var count = await context.Authors.CountAsync();

            Assert.True(result);
            Assert.Equal(1, count);
        }

        [Fact]
        public async Task Delete_Normally_DeletesAnAuthor_ReturnsTrue()
        {
            var context = TestFactory.GetContext();
            var author = new Author() { Name = "Test Author" };
            context.Authors.Add(author);
            await context.SaveChangesAsync();
            var service = new AuthorService(context);

            var result = await service.Delete(author.Id);
            var count = await context.Authors.CountAsync();

            Assert.True(result);
            Assert.Equal(0, count);
        }
    }
}
