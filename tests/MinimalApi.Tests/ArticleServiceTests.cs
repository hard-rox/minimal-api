﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MinimalApi.Application.Interfaces;
using MinimalApi.Domain.Entities;
using MinimalApi.Infrastructure.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace MinimalApi.Tests
{
    //[CollectionDefinition("ArticleServiceTests", DisableParallelization = true)]
    public class ArticleServiceTests
    {
        [Fact]
        public async Task Create_Normally_CreatesAnArticle_ReturnsTrue()
        {
            //Arrange
            var context = TestFactory.GetContext();
            var article = new Article() { Title = "Test", Content = "", Author = new Author() { Name = "Test Author"} };
            var mockLogger = new Mock<ILogger<ArticleService>>();
            var mockCurrentUserService = new Mock<ICurrentUserService>();
            mockCurrentUserService.Setup(x => x.GetUserId()).Returns("test-user");
            mockCurrentUserService.Setup(x => x.GetUsername()).Returns("testUser");
            mockCurrentUserService.Setup(x => x.IsLoggedIn()).Returns(true);
            var service = new ArticleService(context, mockLogger.Object, mockCurrentUserService.Object);

            //Act
            var result = await service.Create(article);
            var count = await context.Articles.CountAsync();

            //Assert
            Assert.True(result);
            Assert.Equal(1, count);
        }

        [Fact]
        public async Task Create_WithoutAuthorId_ThrowsException()
        {
            var context = TestFactory.GetContext();
            var mockLogger = new Mock<ILogger<ArticleService>>();
            var mockCurrentUserService = new Mock<ICurrentUserService>();
            mockCurrentUserService.Setup(x => x.GetUserId()).Returns("test-user");
            mockCurrentUserService.Setup(x => x.GetUsername()).Returns("testUser");
            mockCurrentUserService.Setup(x => x.IsLoggedIn()).Returns(true);
            var service = new ArticleService(context, mockLogger.Object, mockCurrentUserService.Object);
            var article = new Article() { Title = "Test", Content = "" };

            await Assert.ThrowsAsync<DbUpdateException>(() => service.Create(article));
        }

        [Fact]
        public async Task Delete_Normally_DeletesAnArticle_ReturnsTrue()
        {
            var context = TestFactory.GetContext();
            var article = new Article() { Title = "Test", Content = "", Author = new Author() { Name = "Test Author" } };
            context.Articles.Add(article);
            await context.SaveChangesAsync();
            var mockLogger = new Mock<ILogger<ArticleService>>();
            var mockCurrentUserService = new Mock<ICurrentUserService>();
            mockCurrentUserService.Setup(x => x.GetUserId()).Returns("test-user");
            mockCurrentUserService.Setup(x => x.GetUsername()).Returns("testUser");
            mockCurrentUserService.Setup(x => x.IsLoggedIn()).Returns(true);
            var service = new ArticleService(context, mockLogger.Object, mockCurrentUserService.Object);

            var result = await service.Delete(article.Id);

            var count = await context.Articles.CountAsync();
            Assert.True(result);
            Assert.Equal(0, count);
        }
    }
}
