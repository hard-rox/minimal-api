﻿using Microsoft.EntityFrameworkCore;
using MinimalApi.Infrastructure;
using System.Linq;
using Xunit;

//[assembly: CollectionBehavior(DisableTestParallelization = true)]
namespace MinimalApi.Tests
{
    public static class TestFactory
    {
        public static AppDbContext GetContext()
        {
            var options = new DbContextOptionsBuilder()
               //.UseInMemoryDatabase("TestDb")
               .UseSqlServer("Data Source=.;User ID=sa;Password=123456Aa;Initial Catalog=BlogDb-Test;")
               .Options;
            var context = new AppDbContext(options);

            context.Database.EnsureDeleted();
            context.Database.Migrate();

            //var tableNames = context.Model.GetEntityTypes()
            //    .Select(t => t.GetTableName())
            //    .Distinct()
            //    .ToList();
            //tableNames.ForEach(t =>
            //{
            //    context.Database.ExecuteSqlRaw($"DELETE FROM {t};");
            //});

            return context;
        }
    }
}
