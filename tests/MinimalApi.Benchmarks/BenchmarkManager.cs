﻿using BenchmarkDotNet.Running;

namespace MinimalApi.Benchmarks
{
    public static class BenchmarkManager
    {
        public static void RunBenchmarks()
        {
            BenchmarkRunner.Run<AuthorServiceBenchmarks>();
        }
    }
}
