﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using MinimalApi.Application.Interfaces;
using MinimalApi.Domain.Entities;

namespace MinimalApi.Benchmarks
{
    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class AuthorServiceBenchmarks
    {
        //private readonly ICrudService<Author> authorService;

        //public AuthorServiceBenchmarks(ICrudService<Author> authorService)
        //{
        //    this.authorService = authorService;
        //}

        [Benchmark]
        public async Task GetAllAuthors()
        {
            //await authorService.GetAll();

            Thread.Sleep(1000);
        }
    }
}
