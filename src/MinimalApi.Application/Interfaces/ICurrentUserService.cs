﻿namespace MinimalApi.Application.Interfaces
{
    public interface ICurrentUserService
    {
        bool IsLoggedIn();
        string GetUsername();
        string GetUserId();
    }
}
