﻿namespace MinimalApi.Application.Interfaces
{
    public interface ICrudService<T>
    {
        Task<bool> Create(T entity);
        Task<List<T>> GetAll();
        Task<T> Get(int id);
        Task<bool> Update(int id, T entity);
        Task<bool> Delete(int id);
    }
}
