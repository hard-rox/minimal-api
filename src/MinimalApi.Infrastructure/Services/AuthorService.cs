﻿using Microsoft.EntityFrameworkCore;
using MinimalApi.Application.Interfaces;
using MinimalApi.Domain.Entities;

namespace MinimalApi.Infrastructure.Services
{
    public class AuthorService : ICrudService<Author>
    {
        private readonly AppDbContext _appDbContext;

        public AuthorService(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<bool> Create(Author entity)
        {
            await _appDbContext.Authors.AddAsync(entity);
            return await _appDbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _appDbContext.Authors.FirstOrDefaultAsync(x => x.Id == id);
            _appDbContext.Authors.Remove(entity);
            return await _appDbContext.SaveChangesAsync() > 0;
        }

        public async Task<Author> Get(int id)
        {
            return await _appDbContext.Authors.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Author>> GetAll()
        {
            return await _appDbContext.Authors.ToListAsync();
        }

        public async Task<bool> Update(int id, Author entity)
        {
            var oldEntity = await _appDbContext.Authors.FirstOrDefaultAsync(x => x.Id == id);
            oldEntity.Name = entity.Name;
            return await _appDbContext.SaveChangesAsync() > 0;
        }
    }
}
