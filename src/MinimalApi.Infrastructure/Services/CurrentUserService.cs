﻿using MinimalApi.Application.Interfaces;

namespace MinimalApi.Infrastructure.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public string GetUserId()
        {
            return "this-is-dummy-user";
        }

        public string GetUsername()
        {
            return "dummy-user";
        }

        public bool IsLoggedIn()
        {
            return true;
        }
    }
}
