﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MinimalApi.Application.Interfaces;
using MinimalApi.Domain.Entities;

namespace MinimalApi.Infrastructure.Services
{
    public class ArticleService : ICrudService<Article>
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<ArticleService> _logger;
        private readonly ICurrentUserService _currentUserService;

        public ArticleService(AppDbContext appDbContext, ILogger<ArticleService> logger, ICurrentUserService currentUserService)
        {
            _appDbContext = appDbContext;
            _logger = logger;
            _currentUserService = currentUserService;
        }

        public async Task<bool> Create(Article entity)
        {
            await _appDbContext.Articles.AddAsync(entity);
            return await _appDbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _appDbContext.Articles.FirstOrDefaultAsync(x => x.Id == id);
            _appDbContext.Articles.Remove(entity);
            return await _appDbContext.SaveChangesAsync() > 0;
        }

        public async Task<Article> Get(int id)
        {
            return await _appDbContext.Articles.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Article>> GetAll()
        {
            return await _appDbContext.Articles.ToListAsync();
        }

        public async Task<bool> Update(int id, Article entity)
        {
            var oldEntity = await _appDbContext.Articles.FirstOrDefaultAsync(x => x.Id == id);
            oldEntity.Title = entity.Title;
            oldEntity.Content = entity.Content;
            oldEntity.AuthorId = entity.AuthorId;
            return await _appDbContext.SaveChangesAsync() > 0;
        }
    }
}
