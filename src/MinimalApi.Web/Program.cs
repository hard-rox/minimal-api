﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MinimalApi.Application.Interfaces;
using MinimalApi.Benchmarks;
using MinimalApi.Domain.Entities;
using MinimalApi.Infrastructure;
using MinimalApi.Infrastructure.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer("Data Source=.;User ID=sa;Password=123456Aa;Initial Catalog=BlogDb;"));
builder.Services.AddScoped<ICrudService<Author>, AuthorService>();
builder.Services.AddScoped<ICrudService<Article>, ArticleService>();
builder.Services.AddTransient<ICurrentUserService, CurrentUserService>();

var app = builder.Build();

#region authros_endpoints
app.MapGet("/authors", ([FromServices] ICrudService<Author> service) =>
{
    return service.GetAll();
});

app.MapGet("/authors/{id}", ([FromServices] ICrudService<Author> service, int id) =>
{
    return service.Get(id);
});

app.MapPost("/authors", ([FromServices] ICrudService<Author> service, Author author) =>
{
    return service.Create(author);
});

app.MapPut("/authors/{id}", ([FromServices] ICrudService<Author> service, int id, Author author) =>
{
    return service.Update(id, author);
});

app.MapDelete("/authors/{id}", ([FromServices] ICrudService<Author> service, int id) =>
{
    return service.Delete(id);
});
#endregion

#region articles_endpoints
app.MapGet("/articles", ([FromServices] ICrudService<Article> service) =>
{
    return service.GetAll();
});

app.MapGet("/articles/{id}", ([FromServices] ICrudService<Article> service, int id) =>
{
    return service.Get(id);
});

app.MapPost("/articles", ([FromServices] ICrudService<Article> service, Article article) =>
{
    return service.Create(article);
});

app.MapPut("/articles/{id}", ([FromServices] ICrudService<Article> service, int id, Article article) =>
{
    return service.Update(id, article);
});

app.MapDelete("/articles/{id}", ([FromServices] ICrudService<Article> service, int id) =>
{
    return service.Delete(id);
});
#endregion

//BenchmarkManager.RunBenchmarks();

app.Run();